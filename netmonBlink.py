#!/usr/bin/env python

"""

Author: Georgios Lyras
Version: 0.1
Commentary: Should be placed in crontab as a reboot job
Description: Simple script that checks internet connection availability and lights blue for true or red for false.
Package requirements: blinkt libs, and blinkt module of course.
Tested in raspberry pi zero w.

"""

from blinkt import set_pixel, set_brightness, show, clear
import urllib2
import time


def check_connection():
    try:
        urllib2.urlopen('http://www.google.com', timeout=1)
        return True
    except urllib2.URLError as err:
        return False

while True:
    if check_connection():             
        for i in range(8):               
            clear()                      
            set_pixel(i, 50, 50, 255)    
            set_brightness(0.1)          
            show()                       
            time.sleep(3)
    else:
        for i in range(8):
            clear()     
            set_pixel(i, 255, 255, 0)
            set_brightness(0.1)
            show()
            time.sleep(3)

